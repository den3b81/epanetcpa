% Plot attack scenario (water level in the targeted tank)
%
%
% The MIT License
% 
% Copyright (c) 2017 Riccardo Taormina, 
% Singapore University of Technology and Design.
% email: riccardo_taormina@sutd.edu.sg, riccardo.taormina@gmail.com
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.

timeStep = 1; % in hours

% get sensor (water level) and variable (water level = PRESSURE)
sensor = details.sensorToVisualize; variable = details.variable;

% initialize figure
figure()

% truth, no attack
data = returnData(sim0, 'PHY', timeStep); 
ix = find(ismember(data.nodes,sensor)); % get right index for sensor
T = data.T; data = data.(variable);
plot(T(1:end-1),data(1:end-1,ix),'color',[0.3,0.8,0.8],'linewidth',7);
hold on 

% truth
data = returnData(sim1, 'PHY', timeStep); T = data.T; data = data.(variable);
plot(T(1:end-1),data(1:end-1,ix),'color',[0.8,0.8,0.8],'linewidth', 5);

if details.onlyTruth == false
    % plcs 
    marker = {'s','^'};
    colors = {[0.5,0.0,0.25],[0.0,0.25,0.5]};
    for i = 1 : numel(details.PLCs)
        thisPLC = details.PLCs{i};    
        data = returnData(sim1, thisPLC, timeStep); T = data.T; data = data.(variable);    
        plot(T(1:end-1),data(1:end-1,ix),'marker',marker{i},'color',colors{i});
    end

    % SCADA
    data = returnData(sim1, 'SCADA', timeStep); T = data.T; data = data.(variable);
    plot(T(1:end-1),data(1:end-1,ix),'k');
    
    % legend
    legend({'no attack = truth','truth',details.PLCs{:},'SCADA'})
else
    legend({'no attack = truth','truth'})
end

% add title, axis labels and legend
title(details.scenarioName)
xlabel('Time into the simulation [hours]')
ylabel_ = sprintf('Water level in %s [meters]',sensor);
ylabel(ylabel_)
