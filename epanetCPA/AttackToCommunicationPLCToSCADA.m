classdef AttackToCommunicationPLCToSCADA < AttackToCommunication   
    % Class implementing attack to PLC to SCADA connection.
    %
    %
    %
    % The MIT License
    % 
    % Copyright (c) 2017 Riccardo Taormina, 
    % Singapore University of Technology and Design.
    % email: riccardo_taormina@sutd.edu.sg, riccardo.taormina@gmail.com
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in
    % all copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    % THE SOFTWARE.
    %
  
            
    % public methods
    methods
            
    function self = AttackToCommunicationPLCToSCADA(...
            target, ini_condition, end_condition,...
            setting, deact_setting, alterMethod )
        
        self@AttackToCommunication(...
            'SCADA', target, ini_condition, end_condition,...
            setting, deact_setting, alterMethod );
    end        
    
    
    end
    
end
    