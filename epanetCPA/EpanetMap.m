classdef EpanetMap
    % Class for EPANET map. It extends EPANET map to feature a cyber-layer 
    %
    %
    %
    % The MIT License
    % 
    % Copyright (c) 2017 Riccardo Taormina, 
    % Singapore University of Technology and Design.
    % email: riccardo_taormina@sutd.edu.sg, riccardo.taormina@gmail.com
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in
    % all copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    % THE SOFTWARE.
    %   

   
    properties
        originalFilePath    % original file path of .inp file
        
        modifiedFilePath    % modified file path of .inp file
        
        components          % dictionary containing map components
        
        controls            % list of control objects
        
        dummyControls       % list of control objects (dummies)
        
        PLCs                % list of PLC objects
        
        duration            % duration of simulation
        
        baseDemand          % base demand of all nodal junctions
        
        patterns            % demand patterns
        
        tankIniLevels       % tankInitiaLevels
    end
        
    
    % public methods
    methods
        
    function self = EpanetMap(mapFile, patterns, tankIniLevels, PLCinfo)
        
        % patterns asnd tankLevels
        self.patterns = patterns;
        self.tankIniLevels = tankIniLevels;
        
        % original map file
        self.originalFilePath = mapFile; 
                        
        % initialize map and modify .inp file
        self = self.initializeMap();
        
        % open modified file
        EpanetHelper.epanetloadfile(self.modifiedFilePath);   
                                 
        % get all components
        self = getAllComponents(self);
        
        % get all controls
        self = self.getControls();
          
        % construct PLC list
        self = self.setPLCs(PLCinfo);       
                
        % close modified .inp file
        EpanetHelper.epanetclose();        
    end   
       
    function self = getControls(self)
        
        % get number of controls
        nControls = int32(0);
        [~,nControls] = ...
            calllib('epanet2','ENgetcount',...
            EpanetHelper.EPANETCODES('EN_CONTROLCOUNT'),nControls);
        
        % get total number of actionable components
        % (here normal PIPES are not considered actionable)
        nComponents =...
            numel(EpanetHelper.getComponents('PUMPS'))  + ...
            numel(EpanetHelper.getComponents('VALVES')) + ...
            numel(EpanetHelper.getComponents('OF_PIPES'));
        
        % retrieve controls
        self.controls = [];
        for i = 1 : nControls - nComponents
            self.controls = cat(1,self.controls,EpanetControl(i));
        end

        % retrieve dummy controls
        self.dummyControls = [];
        for i = numel(self.controls)+1 : nControls
            self.dummyControls = cat(1,self.dummyControls,EpanetControl(i));
        end
    end 
    
    function self = deactivateControls(self)        
        % cycle through controls and deactivate them
        for i = 1 : numel(self.controls)
            self.controls(i).deactivate();
        end
    end
    
    function [] = setPatterns(self)
        % cycle through all patterns and substitute them
        for i = 1 : size(self.patterns,2)
            errorcode = EpanetHelper.setPattern(i, self.patterns(:,i));
        end
    end
    
    function [] = setInitialTankLevels(self)
        % cycle through all tanks and set initial level
        TANKS = self.components('TANKS');
        EN_TANKLEVEL = EpanetHelper.EPANETCODES('EN_TANKLEVEL');
        for i = 1 : numel(TANKS)
            tankIndex = EpanetHelper.getComponentIndex(TANKS{i});
            errorcode = EpanetHelper.setComponentValue(...
                tankIndex,self.tankIniLevels(TANKS{i}),EN_TANKLEVEL);
        end
    end
    
    function [] = setDuration(self)
%       if ~isnan(self.duration)
        % epanet constant
        % we are considering only hourly patterns here! TO BE MODIFIED
        patStep = single(0);
        [~,patStep] = calllib('epanet2', 'ENgettimeparam',...
            EpanetHelper.EPANETCODES('EN_PATTERNSTEP'), patStep);
        self.duration = size(self.patterns,1) * patStep;        
        calllib('epanet2', 'ENsettimeparam',...
            EpanetHelper.EPANETCODES('EN_DURATION'), single(self.duration));
%         end
    end
    
    end
    
    
    % private methods
    methods (Access = private)
          
    function self = initializeMap(self)
                
        % open original file
        EpanetHelper.epanetloadfile(self.originalFilePath); 
        
        % set modified file path
        [~,ix] = regexp(self.originalFilePath,'\.inp','match');
        self.modifiedFilePath = [self.originalFilePath(1:ix),'inpx'];
        
                
        % add dummy tanks, lines and controls for the tanks which can 
        % overflow (here all are supposed to possibily overflow). 
        % TODO: modify this when if you plat to add new sections 
        % to original .inp file.
        sections = EpanetHelper.addDummyTanks(self.originalFilePath);

        % create temp file
        EpanetHelper.createInpFileFromSections(sections,self.modifiedFilePath)

        % close original file
        EpanetHelper.epanetclose();

        % open temp file for inizialization and to in include additional controls
        EpanetHelper.epanetloadfile(self.modifiedFilePath);

%         % initialize tank levels
%         ixTanks = find(...
%             cellfun(@(x) strcmp(x,'[TANKS]'),{sections.name}));
%         
%         for i = 1 : numel(self.tankIniLevels.keys)
%             thisTank = self.tankIniLevels.keys
%         end
        
        
        
        % Add additional controls for attack
        components = EpanetHelper.getComponents('PUMPS');
        components = cat(1,components,EpanetHelper.getComponents('VALVES'));
        components = cat(1,components,EpanetHelper.getComponents('OF_PIPES'));

        attackControls = {};
        for i = 1 : numel(components)
            thisControl = sprintf('LINK %-6s CLOSED AT TIME 999999999',components{i});
            attackControls{i,1} = thisControl;
        end

        ixControls = find(...
            cellfun(@(x) strcmp(x,'[CONTROLS]'),{sections.name}));
        sections(ixControls).text = cat(1,sections(ixControls).text,' ',attackControls);

        % close temporary file
        EpanetHelper.epanetclose();

        % add controls to temp file (used for simulations)
        EpanetHelper.createInpFileFromSections(sections,self.modifiedFilePath);
                
    end  
    
    function self = setPLCs(self, PLCinfo) 
        % verify if PLCinfo is consistent, i.e. sensors/actuators
        % directly connected only to one PLC and all the sensors/actutators
        % in the controls are connected to PLCs 
        % 
        if isequal(unique([PLCinfo.sensors]), sort([PLCinfo.sensors])) && ...                
            isequal(unique([PLCinfo.actuators]), sort([PLCinfo.actuators]))
            % this checks for uniqueness...
            
            % ... now get all actuators and sensors in controls
            sensors   = {}; actuators = {};
            for i = 1 : numel(self.controls)
                sensors = cat(2,sensors,...
                    EpanetHelper.getComponentId(self.controls(i).nIndex, 1));
                actuators = cat(2,actuators,...
                    EpanetHelper.getComponentId(self.controls(i).lIndex, 0));
            end
            
            % ... and check if are all connected to PLCs
            if isempty(setdiff(unique(sensors), sort([PLCinfo.sensors]))) && ...                
                    isempty(setdiff(unique(actuators), sort([PLCinfo.actuators])));
                fprintf('PLC and controls consistency are consistent. Check PASSED!\n');
            else
                error('Some sensors/actuators in the controls are not linked to PLCs. Check FAILED!');                
            end
        else
            error('Sensors and actuators can only be linked to one PLC. Check FAILED!');                
        end
        
        % if all is ok, then construct PLC list
        self.PLCs = [];
        for i = 1 : numel(PLCinfo)
            self.PLCs = cat(1,self.PLCs,PLC(PLCinfo(i),self.controls));
        end                    
    end
    
    function self = getAllComponents(self)
        % initialize and fill the components dictionary
        componentsTypes = {...
            'TANKS','OF_TANKS','JUNCTIONS','OF_JUNCTIONS',...
            'PUMPS','VALVES','PIPES','OF_PIPES','RESERVOIRS'};

        self.components = containers.Map;
        for i = 1 : numel(componentsTypes)
            fprintf('%s\n',componentsTypes{i})
            self.components(componentsTypes{i}) = EpanetHelper.getComponents(componentsTypes{i});
        end
        
        % check that all labels are unique
        temp = cat(1,self.components.values);
        allComponents = cat(1,temp{:});
        duplicates = setdiff(allComponents,unique(allComponents));
        if ~isempty(duplicates)
            dupLabels = duplicates{1};
            for i = 2 : numel(duplicates)
                dupLables = strcat(...
                    dupLabels,sprintf('\t%s',duplicates{i}));
            end
            error('ERROR: components labels are not unique!!\n%s\n',dupLabels); 
        end
    end
       
    end            
end