classdef AttackToCommunication < CyberPhysicalAttack   
    % This class implements a generic attack targeting a communication
    % channel transmitting sensor data between cyber components.
    %
    %
    %
    % The MIT License
    % 
    % Copyright (c) 2017 Riccardo Taormina, 
    % Singapore University of Technology and Design.
    % email: riccardo_taormina@sutd.edu.sg, riccardo.taormina@gmail.com
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in
    % all copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    % THE SOFTWARE.
    %   

    
    properties
        
        alterMethod             % how is the reading altered (DoS, constant,
                                % offset, function, replicate...)
                                
        alteredReading          % current altered reading of the sensor
        
    end
        
    methods
    % public methods

        
    function self = AttackToCommunication(...
            layer, target, ini_condition, end_condition,...
            setting, deact_setting, alterMethod)
        
        self@CyberPhysicalAttack(...
            layer, target, ini_condition, end_condition,...
            setting, deact_setting);
        
        % initialize other properties
        self.alterMethod    = alterMethod;
        self.alteredReading = NaN;
    end
    
    
    function self = startAttack(self, time, epanetSim)  
        % mark that the attack started
        self.inplace = 1;
        self.iniTime = time;          
        
        % perform attack
        self = self.performAttack(time,epanetSim);
    end
    
    % perform attack
    function self = performAttack(self, time, epanetSim)  
        % compute altered value of the sensor
        self = self.alterReading(time,epanetSim);

    end
        
    % stop attack
    function self = stopAttack(self, time, varargin) 
        % temporary!        
        self.endTime = time;    
        
        
        % get map controls
        controls = varargin{1}.epanetMap.controls;
        dummyControls = varargin{1}.epanetMap.dummyControls;
        
        % attack is off
        self.inplace = 0;

        % TODO: are dummyControls still used for attackstocmmunication?
        % deactivate all dummy controls that had to do with attack
        for i = 1 : numel(self.actControl)
            dummyControls(self.actControl(i)).deactivate();
        end
        
        % ractivate all controls that have to do with sensor        
        for i = 1 : numel(self.deactControls);
            controls(self.deactControls(i)).activate();
        end
        
        % reset altered reading
        self.alteredReading = NaN;
    end   
    
    % evaluate attacks
    function [self, epanetSim] =...
            evaluateAttack(self, epanetSim, tstep)
        if self.inplace == 0
            % attack is not active, check if starting condition met
            flag = self.evaluateStartingCondition(epanetSim.symbolDict);            
            if flag
                % start attack
                self = self.startAttack(epanetSim.symbolDict('TIME'),epanetSim);

            end
        else
            % attack is active
            % check if ending condition is met for this attack
            flag = self.evaluateEndingCondition(epanetSim.symbolDict);
            if flag
                % stop attack
              self = self.stopAttack(epanetSim.symbolDict('TIME'), epanetSim);
            else
                % ...continue attack (needed for sensor alteration)
                self = self.performAttack(epanetSim.symbolDict('TIME'), epanetSim);
            end
        end            
    end
         
    end
    
    % private methods
    methods (Access = private)
          
      function self = alterReading(self, time, epanetSim)   

        % get time vector        
        T = epanetSim.T;
        rowToCopyFrom = numel(T); % initialize to current time
        
        % switch alter method
        switch self.alterMethod
            
            case 'DoS'         
                % reading is not updated
                if isnan(self.alteredReading)
                    % assign last reading if it's first time
                    % otherwise leave unchanged. It's reset to NaN
                    % when attack ceases.                    
                    thisReading = getReading(self, rowToCopyFrom, epanetSim);  
                    self.alteredReading = thisReading;
                end
                
            case 'constant'
                % subsitute reading with a constant value
                self.alteredReading = self.setting;
                
            case 'offset'                
                % adds offset to reading
                thisReading = getReading(self, rowToCopyFrom, epanetSim);  
                self.alteredReading = thisReading + self.setting;
                
            case 'replay'
                % replay attack
                
                % get parameters
                delay = self.setting(1);
                noiseIntensity = self.setting(2);
                maxValue = self.setting(3);
                minValue = self.setting(4);
                
                sPoint = (self.iniTime-delay); % initial copying point
                timeRef = sPoint + mod(T(end)-sPoint,delay);
                rowToCopyFrom = find(T>=timeRef,1);
                % get past reading to repeat
                thisReading = getReading(self, rowToCopyFrom, epanetSim);                   
                % only pressure and water level                                
                delta = noiseIntensity * (2*rand(1)-1);                
                if thisReading + delta > maxValue
                    self.alteredReading = maxValue;
                elseif thisReading + delta < minValue
                     self.alteredReading = minValue; 
                else
                    self.alteredReading = thisReading + delta;
                end   
                
            case 'custom'
                
                % substitute with custom readings
                T  = epanetSim.T(end);                
                ix = find(self.setting(:,1)>=T,1);
                if ~isempty(ix)                
                    self.alteredReading = self.setting(ix,2);      
                else
                    self.alteredReading = self.setting(end,2);
                end
                
            otherwise
                error('not implemented yet!')
        end    
      end
      
      function thisReading = getReading(self, rowToCopyFrom, epanetSim)
        
        time = epanetSim.T(rowToCopyFrom);
        
        % get attacked component and index
        thisComponent = self.target;
        [thisIndex,~,isNode] = EpanetHelper.getComponentIndex(thisComponent);
                
        if isNode
            thisIndex = find(ismember(epanetSim.whatToStore.nodeIdx,thisIndex));
        else            
            thisIndex = find(ismember(epanetSim.whatToStore.linkIdx,thisIndex));
        end
        
        % check if variable has been stored, otherwise return error
        if isempty(thisIndex)
            error('Variable %s is not among those being stored during the simulation',...
                thisComponent);
        end
        
        % If it has been already modified, return the modified value
        % if not, return physical layer reading.
        % TODO: this works properly only if         
        try
            ix = find(([epanetSim.alteredReadings.time] == time) & ...
                strcmp({epanetSim.alteredReadings.layer},'PLC') & ... 
                strcmp({epanetSim.alteredReadings.sensorId},thisComponent));                       
        catch
            % TODO: should substitute this catch statement
            ix = [];
        end
        if ~isempty(ix) 
            thisReading = epanetSim.alteredReadings(ix).reading;
        else
            % check if it's node (if so return pressure)
            if isNode
                thisReading = epanetSim.readings.PRESSURE(rowToCopyFrom,thisIndex);
            else
                % return flow if it's a link
                thisReading = epanetSim.readings.FLOW(rowToCopyFrom,thisIndex);
            end
        end        
    end
        
    end
        
end
    