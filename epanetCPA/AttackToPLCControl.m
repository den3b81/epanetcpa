classdef AttackToPLCControl < CyberPhysicalAttack   
    % Class implementing attack changing an EPANET control.
    %
    %
    % The MIT License
    % 
    % Copyright (c) 2017 Riccardo Taormina, 
    % Singapore University of Technology and Design.
    % email: riccardo_taormina@sutd.edu.sg, riccardo.taormina@gmail.com
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in
    % all copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    % THE SOFTWARE.
    %   
   
        
    properties        
        
        isNode  % are we modifying link or node setting of control?        
        
    end
        
    
    methods
    % public methods
        
    % constructor
    function self = AttackToPLCControl(...
            target, ini_condition, end_condition,...
            setting, deact_setting)
                
        % get the attack target
        % it comes as a string of the format "CTRLxt"
        % where:
        % x     is the number of the attacked control
        % t     is either "n" or "l", depending whether the attack changes 
        %       the controlling node or link setting  
        
        expression = '\d+';
        [temp,ix1,ix2] = regexp(target,expression,'split');
        controlIx = str2num(target(ix1:ix2));        
        
        if ~strcmp(temp{1},'CTRL')
            error('Target must start with CTRL')       
        end
        
        self@CyberPhysicalAttack(...
            'CTRL', controlIx, ini_condition, end_condition,...
            setting, NaN);
        
        % check if is a node or a link setting to be changed
        if strcmp(temp{2},'n')
            self.isNode = true;
        elseif strcmp(temp{2},'l')
            self.isNode = false;
        end        
    end
        
        
    function [self,controls] = startAttack(self, time, epanetSim)                
        % mark that the attack started
        self.inplace = 1;
        self.iniTime = time;          

        % get controls
        controls = epanetSim.epanetMap.controls;
        
        % store original value
        if self.isNode
            self.deact_setting = controls(self.target).nSetting; 
        else
            self.deact_setting = controls(self.target).lSetting; 
        end
        
        % perform attack
        [self, controls] = self.performAttack(time,controls);
    end
    
    function [self,controls] = performAttack(self, time, controls)                        
        if self.isNode
            % change original value (node)
            controls(self.target).nSetting =...
                single(self.setting);
        else
            % change original value (link)
            controls(self.target).lSetting =...
                single(self.setting);
        end

    end
        
    function [self,controls] = stopAttack(self, time, epanetSim) 
        % sto the time
        self.endTime = time;            
        
        % get controls
        controls = epanetSim.epanetMap.controls;
        
        if self.isNode
            % change original value (node)
            controls(self.target).nSetting =...
                single(self.deact_setting);
        else
            % change original value (link)
            controls(self.target).lSetting =...
                single(self.deact_setting);
        end
    end
    
    function [self, epanetSim] =...
            evaluateAttack(self, epanetSim, tstep)
        if self.inplace == 0
            % attack is not active, check if starting condition met
            flag = self.evaluateStartingCondition(epanetSim.symbolDict);            
            if flag
                % start attack
       			[self, epanetSim.epanetMap.controls] = ...
                    self.startAttack(epanetSim.symbolDict('TIME'),epanetSim);
            end
        else
            % attack is active
            % check if ending condition is met for this attack
            flag = self.evaluateEndingCondition(epanetSim.symbolDict);
            if flag
                % stop attack
                [self, epanetSim.epanetMap.controls] = ...
                    self.stopAttack(epanetSim.symbolDict('TIME'),epanetSim);       
            end
        end            
    end
  
    end
    
end
    