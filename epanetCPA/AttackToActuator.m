classdef AttackToActuator < CyberPhysicalAttack
    % Class implementing physical/direct/DoS attacks to an EPANET link.
    %
    %
    %
    % The MIT License
    % 
    % Copyright (c) 2017 Riccardo Taormina, 
    % Singapore University of Technology and Design.
    % email: riccardo_taormina@sutd.edu.sg, riccardo.taormina@gmail.com
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in
    % all copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    % THE SOFTWARE.
    %   

        
    % public methods
    methods
        
    
    function self = AttackToActuator(...
            target, ini_condition, end_condition,...
            setting, deact_setting)   
        
        self@CyberPhysicalAttack(...
            'PHY', target, ini_condition, end_condition,...
            setting, deact_setting )
    end    
    
    
    function self = startAttack(self, time, epanetSim)
        % start attack
        self.inplace = 1;
        self.iniTime = epanetSim.T(end);          
        self = self.performAttack(time, epanetSim);
    end
    
    function [self, epanetSim] = stopAttack(self, time, epanetSim)
        % attack is off
        self.inplace = 0;
        
        % deactivate dummy control
        epanetSim.epanetMap.dummyControls(self.actControl) = ...
            epanetSim.epanetMap.dummyControls(self.actControl).deactivateWithValues(...
            int64(time));

    end
    
    function self = performAttack(self, time, epanetSim)
        % perform attack
        
        % get dummy controls
        dummyControls = epanetSim.epanetMap.dummyControls;
 
        % get attacked component and index
        thisComponent = self.target;
        thisIndex = EpanetHelper.getComponentIndex(thisComponent);
        
        % activate dummy control, save and exit        
        for i = 1 : numel(dummyControls)
            if dummyControls(i).lIndex == thisIndex
                self.actControl = i;
                return
            end  
        end                
    end
    
    function [self, epanetSim] = evaluateAttack(self, epanetSim, tstep)
        
        if self.inplace == 0
            % attack is not active, check if starting condition met
            flag = self.evaluateStartingCondition(epanetSim.symbolDict);            
            if flag
                % start attack
                self = self.startAttack(...
                    int64(epanetSim.simTime + tstep),epanetSim);
                try
                    % TO DO: should find a better way to perform dummy
                    % control activation, outside of this class.
                    epanetSim.epanetMap.dummyControls(self.actControl).isActive = 1;                     
                catch
                    error('UNEXPECTED EXCEPTION!')
                end

            end
        else
            % attack is active, check if ending condition is met for this attack
            flag = self.evaluateEndingCondition(epanetSim.symbolDict);
            if flag
                % stop attack
                [self, epanetSim] = self.stopAttack(int64(epanetSim.simTime + tstep), epanetSim);
            end
        end
    end
    
    % evaluate ending condition
    function flag = evaluateEndingCondition(self, symbolDict)       
        % maybe can couple with previous one?!
        
        % get condition
        thisCondition = self.end_condition;
        % find vars
        vars = symvar(thisCondition);
        % evaluate each var
        for j = 1 : numel(vars)
            thisVar = vars{j};
            eval(sprintf('%s = symbolDict(''%s'');',thisVar,thisVar));
        end
        % evaluate condition        
        flag = eval([thisCondition,';']);
    end
    
    end
    
end
    