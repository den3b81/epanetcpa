classdef CyberPhysicalAttack
    % Basci class for implementing cyber physical attacks
    %
    %
    %
    % The MIT License
    % 
    % Copyright (c) 2017 Riccardo Taormina, 
    % Singapore University of Technology and Design.
    % email: riccardo_taormina@sutd.edu.sg, riccardo.taormina@gmail.com
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in
    % all copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    % THE SOFTWARE.
    %   
  
    
    properties
        layer           % targeted layer (PHY,PLC,SCADA)
        target          % target component/comm. link          
        ini_condition   % statement to check for attack start
        end_condition   % statement to check for attack end
        actControl      % control used for simulation
        setting         % control setting
        deact_setting   % control setting (when deactivated)
        deactControls   % controls deactivated for simulation
        deactSettings   % settings of deactivated controls
        inplace         % 0 (no attack) or 1 (attack on)        
        iniTime         % when the attack starts
        endTime         % when the attack ends
    end
        
    
    % public methods
    methods
        
    % constructor
    function self = CyberPhysicalAttack(...
            layer, target, ini_condition, end_condition,...
            setting, deact_setting )
        
        self.layer  = layer;
        self.target = target;
        self.ini_condition = ini_condition;
        self.end_condition = end_condition;        
        self.setting       = setting;
        self.deact_setting = deact_setting;

        
        self.actControl    = [];
        self.deactControls = [];
        self.deactSettings = [];
        self.inplace = 0;        
    end
    
    %% PROTOTYPES
   
    % evaluate attack
    function self = evaluateAttack(varargin)
         % this is a prototype method!        
        error('Implement this method for subclass of CyberPhysicalAttack!')       
    end
    
    % start attack
    function self = startAttack(varargin)       
        % this is a prototype method!        
        error('Implement this method for subclass of CyberPhysicalAttack!')
    end
    
    % stop attack
    function self = stopAttack(varargin)       
        % this is a prototype method!        
        error('Implement this method for subclass of CyberPhysicalAttack!')
    end
    
    % perform attack
    function self = performAttack(varargin)       
        % this is a prototype method!        
        error('Implement this method for subclass of CyberPhysicalAttack!')
    end
    

%%   
    % evaluate starting condition
    function flag = evaluateStartingCondition(self, symbolDict)       
        % Checks whether condition to start attack is verified or not.
        % Uses the dictionary of symbols evaluated by EpanetSimulation.
        
        % get condtion
        thisCondition = self.ini_condition;
        % find vars
        vars = symvar(thisCondition);
        % evaluate each var
        for j = 1 : numel(vars)
            thisVar = vars{j};
            eval(sprintf('%s = symbolDict(''%s'');',thisVar,thisVar));
        end
        % evaluate condition
        flag = eval([thisCondition,';']);          
    end
    
    % evaluate ending condition
    function flag = evaluateEndingCondition(self, symbolDict)       
        % THIS METHOD SHOULD BE REMOVED OR OVERRIDDEN
        
        % maybe can couple with previous one?!
        
        % get condition
        thisCondition = self.end_condition;
        % find vars
        vars = symvar(thisCondition);
        % evaluate each var
        for j = 1 : numel(vars)
            thisVar = vars{j};
            eval(sprintf('%s = symbolDict(''%s'');',thisVar,thisVar));
        end
        % evaluate condition        
        flag = eval([thisCondition,';']);

        % mark that the attack has ended
        self.inplace = 0;
        self.endTime = symbolDict('TIME');
    end
 
    end
end
    