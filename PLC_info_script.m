% PLC info describes PLC domains (i.e. sensors attached and actuators
% controlled) for CTOWN
%
%
%
% The MIT License
% 
% Copyright (c) 2017 Riccardo Taormina, 
% Singapore University of Technology and Design.
% email: riccardo_taormina@sutd.edu.sg, riccardo.taormina@gmail.com
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.

PLCinfo(1).name         = 'PLC1';
PLCinfo(1).sensors      = {};
PLCinfo(1).actuators    = {'PU1','PU2','PU3'};

PLCinfo(2).name         = 'PLC2';
PLCinfo(2).sensors      = {'T1'};
PLCinfo(2).actuators    = {};

PLCinfo(3).name         = 'PLC3';
PLCinfo(3).sensors      = {'T2'};
PLCinfo(3).actuators    = {'PU4','PU5','PU6','PU7','V2'};

PLCinfo(4).name         = 'PLC4';
PLCinfo(4).sensors      = {'T3'};
PLCinfo(4).actuators    = {};

PLCinfo(5).name         = 'PLC5';
PLCinfo(5).sensors      = {};
PLCinfo(5).actuators    = {'PU8','PU9','PU10','PU11'};

PLCinfo(6).name         = 'PLC6';
PLCinfo(6).sensors      = {'T4'};
PLCinfo(6).actuators    = {};

PLCinfo(7).name         = 'PLC7';
PLCinfo(7).sensors      = {'T5'};
PLCinfo(7).actuators    = {};

PLCinfo(8).name         = 'PLC8';
PLCinfo(8).sensors      = {'T6'};
PLCinfo(8).actuators    = {};

PLCinfo(9).name         = 'PLC9';
PLCinfo(9).sensors      = {'T7'};
PLCinfo(9).actuators    = {};
