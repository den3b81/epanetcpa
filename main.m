%% Examples of cyber-physical attacks on WADI testbed
%
%
% The MIT License
% 
% Copyright (c) 2017 Riccardo Taormina, 
% Singapore University of Technology and Design.
% email: riccardo_taormina@sutd.edu.sg, riccardo.taormina@gmail.com
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.

%% INITIALIZATION
clear; 
clc;

% add path for epanetCPA toolbox
addpath('.\epanetCPA\')

% add location of the map file 
originalFilePath = 'CTOWN.inp';

% script to retrieve PLC i;
PLC_info_script

% select attack scenario[from 1 to 6]
scenarioNumber = 2;
[attacks, details, randomSeed] = selectAttack(scenarioNumber);

% initialize random number generator
rng(randomSeed,'v5uniform')

% load pattern
load('5patterns_3days.mat','patterns')

% get initial tank values
initialTankLevels = initializeTanks(originalFilePath);

%% CREATE AND RUN SIMULATIONS

% create Map
theMap = EpanetMap(originalFilePath,patterns,...
    initialTankLevels, PLCinfo);

% create and run simulation with no attack
sim0 = EpanetSimulation(theMap,[],10,details.whatToStore);
sim0 = sim0.run();

% create and run simulation with attacks
sim1 = EpanetSimulation(theMap,attacks,10,details.whatToStore);
sim1 = sim1.run();

% plot
plot_attack_scenario
