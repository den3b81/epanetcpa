function [attacks, details, randomSeed] = selectAttack(attackNumber)
% Example attack scenarios
%
%
% The MIT License
% 
% Copyright (c) 2017 Riccardo Taormina, 
% Singapore University of Technology and Design.
% email: riccardo_taormina@sutd.edu.sg, riccardo.taormina@gmail.com
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.

% initialize
name = 'noname';
variable = '';
PLCs = {}; 
sensorToVisualize = {''};
onlyTruth = false; % == true for only truth readings

switch attackNumber
            
    case 1
        % The attacker turns PU1 off until T1 is almost dry.    
        attacks{1} = AttackToActuator(...
            'PU1', 'TIME == 20','T1<0.25', 0, 1);      
        
        whatToStore.sensors  = {'T1','PU1','PU2'};
        whatToStore.nodeVars = {'PRESSURE'};
        whatToStore.linkVars = {'FLOW'};
        
        % visualization
        name = 'PU1 off';
        sensorToVisualize = 'T1';
        variable = 'PRESSURE';
        PLCs = {'PLC1'};
        
        % seed for random number generator (for reproducibility)
        randomSeed = 312;
    
    case 2        
        % T5 sensor substituted with one returning custom readings.
        t = 40:72; y = [repmat([4.5,4.5,4.5,4.5,1,1,1,1,1,1],1,3),1,1,1];
        attacks{1} = AttackToSensor(...
            'PLC7(T5)', 'TIME == 40','TIME == -1', [t;y]',[], 'custom');            
        whatToStore.sensors  = {'T5','PU10','PU11'};
        whatToStore.nodeVars = {'PRESSURE'};
        whatToStore.linkVars = {'FLOW'};
        
        % info visualization
        name = 'Sensor T5 substituted.';
        sensorToVisualize = 'T5';
        variable = 'PRESSURE';
        PLCs = {'PLC7','PLC5'};
        
        % seed for random number generator (for reproducibility)
        randomSeed = 984;
    
    case 3        
        % DoS of channel carrying T1 sensor readings to PLC1. PLC1 does not
        % receive updated readings and therefore fails to shut off the two
        % pumps feeding water to T1. A replay attack is carried out on the
        % communication to SCADA to prevent operators or detection algorithms
        % from discovering the 1st attack.
        
        % DoS channel carrying T1 sensor readings to PLC1    
        attacks{1} = AttackToCommunicationPLCToPLC(...
            'PLC1(T1)', 'TIME == 40','TIME == -1', [],[], 'DoS');      

        % Alter channel carrying T1 sensor readings to SCADA    
        attacks{2} = AttackToCommunicationPLCToSCADA(...
            'T1', 'TIME == 39 ','TIME == -1', [38,0,5,0],[], 'replay');      
   
        whatToStore.sensors  = {'T1','PU1','PU2'};
        whatToStore.nodeVars = {'PRESSURE'};
        whatToStore.linkVars = {'FLOW'};
        
        % info visualization
        name = 'DoS + SCADA Deception, tank T1';
        sensorToVisualize = 'T1';
        variable = 'PRESSURE';
        PLCs = {'PLC1','PLC2'};
        
        % seed for random number generator (for reproducibility)
        randomSeed = 100;

    case 4
        % The attacker prevents valve V2 to close by manipulating T3 sensor
        % readings arriving to PLC3. The costant value is small enough
        % to keep V2 open.
        
        % Alteration using constat value. V2 > 0 implies V2 is open.
        attacks{1} = AttackToCommunicationSensorToPLC(...
            'PLC3(T2)', 'TIME > 40 && V2 > 0','TIME == -1', 1,[], 'constant');      
   
        whatToStore.sensors  = {'T2','V2'};
        whatToStore.nodeVars = {'PRESSURE'};
        whatToStore.linkVars = {'FLOW'};
        
        % visualization
        name = 'V2 forced open via deception';
        sensorToVisualize = 'T2';
        variable = 'PRESSURE';
        PLCs = {'PLC3'};
        
        % seed for random number generator (for reproducibility)
        randomSeed = 100;
        
    case 5
        % Similar to scenario #4 but with tank overflow
        
        % Alteration using constat value. V2 > 0 implies V2 is open.
        attacks{1} = AttackToCommunicationSensorToPLC(...
            'PLC3(T2)', 'TIME > 40 && V2 > 0','TIME == -1', 1 , [], 'constant');      
   
        % enable tank T2 to overflow 
        % OFpT2 is the pipe connecting the network with the dummy tank
        %       simulating overflow (OFT2).
        % ATT1  is == TRUE if attacks{1} is inplace. FALSE otherwise.
        % MAXLEVELT2 identifies the maximum level of T2
        
        attacks{2} = AttackToActuator('OFpT2', 'ATT1 && T2 > MAXLEVELT2',...
            'T2 <= MAXLEVELT2', 1, 0);    
   
        whatToStore.sensors  = {'T2','OFT2','V2','OFpT2'};
        whatToStore.nodeVars = {'PRESSURE'};
        whatToStore.linkVars = {'FLOW'};
        
        % visualization
        name = 'V2 forced open via deception + overflow';
        sensorToVisualize = 'OFT2';
        variable = 'PRESSURE';
        onlyTruth = true;
        
        % seed for random number generator (for reproducibility)
        randomSeed = 100;

    
    case 6      
        % The adversary attacks PLC3 and modifies the control settings that
        % regulate PU4 and PU5 operations.
        
        attacks{1} = AttackToPLCControl(...
            'CTRL7n', 'TIME == 20', 'TIME == 60', 0.25, NaN);
        attacks{2} = AttackToPLCControl(...
            'CTRL8n', 'TIME == 20','TIME == 60', 2, NaN);
        attacks{3} = AttackToPLCControl(...
            'CTRL9n', 'TIME == 20', 'TIME == 60', 0.25, NaN);
        attacks{4} = AttackToPLCControl(...
            'CTRL10n', 'TIME == 20','TIME == 60', 2, NaN);  
        
        whatToStore.sensors  = {'T3','PU4','PU5'};
        whatToStore.nodeVars = {'PRESSURE'};
        whatToStore.linkVars = {'FLOW'};
        
        % visualization
        name = 'Attack of PLC3 controls for PU4 and PU5. Low level in T3';
        sensorToVisualize = 'T3';
        variable = 'PRESSURE';
        PLCs = {'PLC4'};
        
        % seed for random number generator (for reproducibility)
        randomSeed = 6509;
    
    otherwise
        error('This attack has not been implemented!')
end

% structure details
details.scenarioName = name;
details.sensorToVisualize = sensorToVisualize;
details.variable = variable;
details.whatToStore = whatToStore;
details.PLCs = PLCs;
details.onlyTruth = onlyTruth;