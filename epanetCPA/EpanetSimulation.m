classdef EpanetSimulation
    % Class for running EPANET simulation with and without attacks
    %    
    %
    % The MIT License
    % 
    % Copyright (c) 2017 Riccardo Taormina, 
    % Singapore University of Technology and Design.
    % email: riccardo_taormina@sutd.edu.sg, riccardo.taormina@gmail.com
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in
    % all copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    % THE SOFTWARE.
    %   

    
    properties
        epanetMap           % EpanetMap instance contaning the modified .inp file;
        
        attacks             % List of cyber-physical attacks (if empty, load original map)                
                
        display_every       % 0 = no display, otherwise display_ever x iterations
        
        startTime           % startTime of the simulation
        
        simTime             % current time into simulation        
        
        readings            % readings of the simulation                        
        
        symbolDict          % dictionary with all symbols needed attack begin/stop
        
        T, cT               % array of times and clocktimes
        
        attackTrack         % track attack history (on/off for each attack, in time)                
        
        alteredReadings     % here are the information regarding the altered readings at PLC and SCADA layer
        
        whatToStore         % readings and variables to store        
    end
    
    
    % public methods
    methods
        
    function self = EpanetSimulation(epanetMap, attacks, display_every, whatToStore)        
        % Constructor class for EpanetSimulation.
        %
        % Usage:
        % 
        % EpanetSimulation(epanetMap, attacks, display_every,whatToStore)
        % 
        % where
        %
        % > epanetMap     is an epanetMap object containing the WDS map 
        % > attacks       is a cell array containing the attacks
        % > display_every is an int regulating verbosity 
        % > whatToStore   is a struct containing which nodes/links/variables to store
        %                 during the simulation
        %
        % Returns an EpanetSimulation object.
        
        %% 
        % fill properties
        self.epanetMap      = epanetMap;
        self.attacks        = attacks; 
        self.display_every  = display_every;
        self.whatToStore    = whatToStore;
        
        % load map file
        EpanetHelper.epanetloadfile(self.epanetMap.modifiedFilePath);
        
        %% get nNodes and nLinks (maybe you should store this somewhere...)
        % *** TO DO ***
        nNodes = 0; nLinks = 0;
        [~,nNodes] = calllib(...
            'epanet2','ENgetcount', EpanetHelper.EPANETCODES('EN_NODECOUNT'),nNodes);
        [~,nLinks] = calllib(...
            'epanet2','ENgetcount', EpanetHelper.EPANETCODES('EN_LINKCOUNT'),nLinks);
        
        %% what to store (put this somewhere else)
        % *** TO DO ***
        if isempty(whatToStore)
            self.whatToStore.sensors = {'all'};
            % we will store readings for all nodes
            self.whatToStore.nodeIdx = 1 : nNodes;
            % ... and all links
            self.whatToStore.linkIdx = 1 : nLinks;            
            % store all variables as well
            self.whatToStore.nodeVars = {'PRESSURE','HEAD','DEMAND'};
            self.whatToStore.linkVars = {'FLOW', 'STATUS', 'SETTING','ENERGY'};            
        else
            self.whatToStore.nodeIdx = [];
            self.whatToStore.linkIdx = [];
            switch whatToStore.sensors{1}
                case 'all'
                    % we will store readings for all nodes
                    self.whatToStore.nodeIdx = 1 : nNodes;
                    % ... and all links
                    self.whatToStore.linkIdx = 1 : nLinks; 
                case 'all nodes'
                    % we will store readings for all nodes
                    self.whatToStore.nodeIdx = 1 : nNodes;
                case 'all links'
                    % we will store readings for all links
                    self.whatToStore.linkIdx = 1 : nLinks; 
                otherwise
                    for i = 1 : numel(whatToStore.sensors)
                        thisId = whatToStore.sensors{i};
                        [thisIdx, ~, isNode] = EpanetHelper.getComponentIndex(thisId);
                        if isNode
                            self.whatToStore.nodeIdx = cat(1,self.whatToStore.nodeIdx,thisIdx);
                        else
                            self.whatToStore.linkIdx = cat(1,self.whatToStore.linkIdx,thisIdx);
                        end
                    end
            end
        end
        
        % get stored nodes and links IDs *** TO DO ***
        self.whatToStore.nodeID = cell(size(self.whatToStore.nodeIdx,1),1);
        self.whatToStore.linkID = cell(size(self.whatToStore.linkIdx,1),1);
        for j = 1 : numel(self.whatToStore.nodeIdx)
            thisIdx = self.whatToStore.nodeIdx(j);
            self.whatToStore.nodeID{j} = EpanetHelper.getComponentId(thisIdx,1);
        end
        
        for j = 1 : numel(self.whatToStore.linkIdx)
            thisIdx = self.whatToStore.linkIdx(j);
            self.whatToStore.linkID{j} = EpanetHelper.getComponentId(thisIdx,0);
        end
        

        %% INITIALIZATION
        
        % initialize time
        self.simTime = 0;      

        % get starttime constant
        HOURS_TO_SECONDS = 3600;
        STARTTIME = int64(0);
        [~,STARTTIME] = calllib(...
            'epanet2','ENgettimeparam', ...
            EpanetHelper.EPANETCODES('EN_REPORTSTART'),STARTTIME);
        self.startTime = double(STARTTIME/HOURS_TO_SECONDS);

        % initialize readings arrays
        self.readings.PRESSURE = []; self.readings.DEMAND = []; 
        self.readings.FLOW     = []; self.readings.SETTING = []; 
        self.readings.ENERGY   = []; self.readings.HEAD = []; 
        self.readings.STATUS   = [];

        % initialize time, clocktime and attack track arrays
        self.T = []; self.cT = []; self.attackTrack = [];                             

        % initialize symbol dictionary
        self = initializeSymbolDictionary(self); % <--- include in initialize?

        % initialize altered readings
        self.alteredReadings = [];

        %%
        EpanetHelper.epanetclose();   
    end        
    
    function self = run(self)       
        % Method that runs the simulation 
        % (only hydraulic, water quality to be added later).
        %
        % Usage: sim.run(), where sim is an EpanetSimulation object.
        
        % open simulation
        EpanetHelper.epanetloadfile(self.epanetMap.modifiedFilePath);
        
        %% INITIALIZE MAP        
        % *** TO DO ***, put this block somewhere else
        
        % set patterns
        self.epanetMap.setPatterns();
        
        % set simulation duration
        self.epanetMap.setDuration();
        
        % set initial tank levels
        self.epanetMap.setInitialTankLevels();
                        
        % deactivate all map controls
        self.epanetMap.deactivateControls();
             
        %% MAIN LOOP
        % Open the hydraulic solver
        errorcode = calllib('epanet2', 'ENopenH');
        
        % Initialize the hydraulic solver
        INITFLAG  = 0; % change this to 1 when implementing WaterQuality        
        errorcode = calllib('epanet2', 'ENinitH', INITFLAG);
        HOURS_TO_SECONDS = 3600;

        % Simulation loop
        tstep  = 1;
%         strlen = 0;
        while tstep && ~errorcode 
            TIME = double(self.simTime)/HOURS_TO_SECONDS;
            if (TIME>0) && mod(TIME,self.display_every)<=0.00001% TODO: do this better
                echoString = sprintf('TIME: %.3f\n',TIME);
                fprintf(echoString);
            end 
            
%             if ~isempty(self.attacks)
                % do simulation step
            [self,tstep] = self.hydraulicStep(tstep); 
%             end
            
            % run hydraulics simulation step
            [~, self.simTime] = calllib('epanet2', 'ENrunH', self.simTime);
            
            % update simulation state
            self = self.getCurrentState();  

            % continue to the next time step ...
            [errorcode, tstep] = calllib('epanet2', 'ENnextH', tstep);        
        end
        
        %% close simulation
        EpanetHelper.epanetclose();
    end
    
    function dataStruct = returnData(self,layer,deltaT)
        % Returns data stored during the simulation for a given layer (PHY,
        % PLCs, SCADA) at given time intervals (deltaT). 
        %
        % Examples:  
        %
        % sim.returnData('PHY',5) 
        %   --> returns real hydraulic data from the system at every hour.
        %       (no altered readings)
        % 
        % sim.returnData('SCADA',1) 
        %   --> returns data from SCADA layer measured every hour.
        %
        % sim.returnData('PLC1',1) 
        %   --> returns data from PLC1 measured every hour.
        %
        %
        % The method returns a struct with the following fields:
        % > layer       the layer where the data is stored
        % > nodes       the nodes monitored at that layer
        % > links       the links monitored at that layer  
        % > T           array of timesteps
        % > 1 field for each stored variable (PRESSURE, FLOW, ...) with as
        %   many rows as T and columns as the number of nodes/links stored.
        %
        %
        
        % Check if simulation ran already
        if isempty(self.T)
            error('The simulation has yet to run!');
        end
        
        % get layer
        % *** TO DO ***, is it layer the right name?
        dataStruct.layer = layer;    

        switch layer
            case 'PHY'
                % get all data at PHYSICAL layer
                nodes = self.whatToStore.nodeID;
                links = self.whatToStore.linkID;        
            case 'SCADA'
                % get all data at SCADA layer
                nodes = self.whatToStore.nodeID;
                links = self.whatToStore.linkID;        
            otherwise
                % has to be a PLC. get PLC nodes and links
                PLCix = find(ismember({self.epanetMap.PLCs.name},layer));
                if isempty(PLCix)
                    error('Layer : %s not found.', layer);
                end           
                thisPLC = self.epanetMap.PLCs(PLCix);
                nodes = cat(1,thisPLC.sensors, thisPLC.sensorsIn);
                links = thisPLC.actuators;                
        end

        % get data
        iniT = self.T(1); endT = self.T(end);
        times = iniT:deltaT:endT;
        dataStruct.T = times';
        ixes = ismember(self.T,times)';
        nodeIxes = ismember(self.whatToStore.nodeID, nodes);
        linkIxes = ismember(self.whatToStore.linkID, links);

        for i = 1 : numel(self.whatToStore.nodeVars)    
            thisVar = self.whatToStore.nodeVars{i};    
            data = self.readings.(thisVar)(:,nodeIxes);
            dataStruct.(thisVar) = data(ixes,:);
        end

        for i = 1 : numel(self.whatToStore.linkVars)
            thisVar = self.whatToStore.linkVars{i};
            data = self.readings.(thisVar)(:,linkIxes);
            dataStruct.(thisVar) = data(ixes,:);
        end

        % alter data
        if strcmp(layer,'PHY')~=1 && ~isempty(self.alteredReadings)
            % substitute altered readings
            ixesLayer = ismember({self.alteredReadings.layer},layer);
            if sum(ixesLayer) > 0
                % get altered sensors
                alteredSensors = unique({self.alteredReadings(ixesLayer).sensorId});
                for i = 1 : numel(alteredSensors)
                    thisSensor = alteredSensors(i);            
                    ixesSensor = ixesLayer & ismember({self.alteredReadings.sensorId},thisSensor);
                    isNode = true;
                    sensorPos = find(ismember(nodes,thisSensor));            
                    if isempty(sensorPos)
                        isNode = false;
                        sensorPos = find(ismember(links,thisSensor));
                    end
                    % get time and readings
                    T_ = [self.alteredReadings(ixesSensor).time];
                    alterIxes = ismember(times,T_);
                    alterIxes_ = ismember(T_,times);
                    r_ = [self.alteredReadings(ixesSensor).reading];            
                    if isNode
                        % alter pressure/water level readings
                        dataStruct.PRESSURE(alterIxes,sensorPos) = r_(alterIxes_);
                    else
                        % alter flow
                        dataStruct.FLOW(alterIxes,sensorPos) = r_(alterIxes_);
                        if sum(ismember(self.whatToStore.linkVars,'STATUS'))>0 
                            % alter status too, if being tracked=['p
                            dataStruct.STATUS(alterIxes,sensorPos) = r_(alterIxes_)>0;
                        end                    
                    end                                    
                end
            end
        end

        dataStruct.nodes = nodes';
        dataStruct.links = links';
        
        % *** TO DO ***, handle attack track
    end
    
    % end of public methods
    end           
    
    
    % private methods
    methods (Access = private)
        
    % execute hydraulic step
    function [self,tstep] = hydraulicStep(self, tstep) 
        % This method executes an hydraulic step using the EPANET toolkit.
        
        %% Initialize dictionaries            
        % update dictionary
        self = updateSymbolDictionary(self);
        
        % PLC dictionaries. get all PLCs
        PLCs = self.epanetMap.PLCs;
        for j = 1 : numel(PLCs)
            % get PLC name
            PLCname = PLCs(j).name;
            % intialize dict for this PLC
            eval(sprintf('%sdict = containers.Map();', PLCname)); 
            % sensors read
            sensors = cat(1,PLCs(j).sensors,PLCs(j).sensorsIn);
            for k = 1 : numel(sensors)
                sensor = sensors{k};
                try
                    reading = self.symbolDict(sensor);
                catch
                    % fprintf('Sensor %s not used in control logic:
                    % skipping.',sensor)
                end
                eval(sprintf('%sdict(sensor) = reading;',PLCname));
            end
        end
        
        % create SCADA dictionary
        SCADAdict = [self.symbolDict;containers.Map()]; % concatenation so it's deep-copy        
        
        %% Evaluate, perform and track attacks
        
        % get number of attacks
        nAttacks   = numel(self.attacks);  

        % counter for attacks in place
        attacksInPlace = 0;
        
        % Cycle through attacks
        for i = 1 : nAttacks
            % for each attack, check their conditions and see whether they
            % have to start, end or continue...
            [attack, self] = self.attacks{i}.evaluateAttack(self, tstep);
            
            % alter readings if attack is in place and targets
            % sensors/communications
            if attack.inplace && isprop(attack,'alteredReading')
                % get layer and target
                layer  = attack.layer;
                target = attack.target;
                alteredReading = attack.alteredReading;
                
                % create new entry for altered readings
                self = self.storeAlteredReadingEntry(...
                    layer, target, alteredReading);
                
                if strcmp(layer,'SCADA') == 1
                    % Modify SCADA readings dictionary
                    SCADAdict(target) = alteredReading;

                else
                    % Attack at PLC layer
                    thisPLC = PLCs(ismember({PLCs.name},layer));
                    PLCname = thisPLC.name;
                    
                    % modify PLC dictionary
                    eval(sprintf('%sdict(target) = alteredReading;', PLCname));
                                     
                    if sum(ismember(thisPLC.sensors,target))>0
                        % The following is done only if we are attacking
                        % which is reading the sensor. This will
                        % automatically alter readings also for other PLC
                        % receiving this sensor and SCADA.
                    
                        % modify dictionary of PLCs receiving the sensor
                        % readings (sent by the attacked PLC)
                        for j = 1 : size(PLCs,1)
                            PLCname_ = PLCs(j).name;
                            if (strcmp(PLCname_,PLCname) == 0) && ...
                                    (sum(ismember(PLCs(j).sensorsIn,target)) > 0)
                                % Modify PLC dictionary and create new
                                % entry for altered readings
                                eval(sprintf('%sdict(target) = alteredReading;', PLCname_));                                  
                                self = self.storeAlteredReadingEntry(...
                                    PLCname_, target, alteredReading);                                
                            end
                        end
                        
                        % Modify SCADA readings dictionary and create new
                        % entry for altered readings
                        SCADAdict(target) = alteredReading;
                        self = self.storeAlteredReadingEntry(...
                            'SCADA', target, alteredReading);  

                        % ... and update overall PLC dict
                        PLCdict(target) = alteredReading;                        
                    end
                end
            end

            % store attacks
            self.attacks{i} = attack;
            
            % update count of attacks in place
            attacksInPlace = attacksInPlace + attack.inplace;                                        
        end
                         
        
        %% Override control logic
        for i = 1 : numel(PLCs)
            thisPLC = PLCs(i);
            eval(sprintf('PLCdict = %sdict;',thisPLC.name));
            % this doesn't look as nice as if it were a method within the
            % EpanetMap class, or PLC better
            self.overrideControls(thisPLC, PLCdict);
        end
        
        % activate dummy controls if needed
        self = self.activateDummyControls(tstep);
                
        %% Track attack history
        attackFlag = zeros(1,nAttacks);
        for i = 1 : nAttacks
            attackFlag(i) = self.attacks{i}.inplace;      
        end
        self.attackTrack = cat(1,self.attackTrack,attackFlag);

        end
    
    function self = getCurrentState(self)
        % store time, nodes/links readings
        
        % *** TO DO ***, rename it? Put HOURS_TO_SECONDS somewhere else?

        % store time vars
        HOURS_TO_SECONDS = 3600;
        TIME = double(self.simTime)/HOURS_TO_SECONDS;
        % time
        self.T = cat(1,self.T,TIME);
        % ... and clocktime
        self.cT = cat(1,self.cT,mod(self.startTime+TIME,24));
        
        % store nodal readings
        variables = self.whatToStore.nodeVars;
        for j = 1 : numel(variables)
            nNodes = numel(self.whatToStore.nodeIdx);
            this_var = variables{j};
            value = repmat(0.0,1,nNodes);
            index = self.whatToStore.nodeIdx;
            for n = 1:nNodes                
                [errorcode, value(n)] = calllib('epanet2', 'ENgetnodevalue', index(n),...
                    EpanetHelper.EPANETCODES(['EN_',variables{j}]), value(n));
            end
            self.readings.(this_var) = cat(1,self.readings.(this_var),double(value));
        end
        
        % store link readings
        variables = self.whatToStore.linkVars;
        for j = 1 : numel(variables)
            nLinks = numel(self.whatToStore.linkIdx);
            this_var = variables{j};
            value = repmat(0.0,1,nLinks);
            index = self.whatToStore.linkIdx;
            for n = 1:nLinks
                [errorcode, value(n)] = calllib('epanet2', 'ENgetlinkvalue', index(n),...
                    EpanetHelper.EPANETCODES(['EN_',variables{j}]), value(n));
            end
            self.readings.(this_var) = cat(1,self.readings.(this_var),double(value));
        end

    end    
    
    function self = initializeSymbolDictionary(self)
        % initialize symbol dictionary
        
        % *** TO DO ***, merge it with some initializeSimulation of sorts??
        % remove EN_MAXLEVEL

        % initialize
        self.symbolDict = containers.Map();
        
        % add TANKS max levels (these won't change during simulation) get
        % list of tanks
        EN_MAXLEVEL = 21;
        TANKS = self.epanetMap.components('TANKS'); 
        for i = 1 : numel(TANKS)
            thisTankIndex = EpanetHelper.getComponentIndex(TANKS{i});
            maxTankLevel  = EpanetHelper.getComponentValue(thisTankIndex, true, EN_MAXLEVEL);
            maxTankLevel  = maxTankLevel - 10^-3; % ... minus a bit or it won't work
            % create parsing CONSTANT
            eval(sprintf('self.symbolDict(''%s%s'') = %d;',...
                'MAXLEVEL',TANKS{i},maxTankLevel));   
        end
        clear TANKS
    end
    
    function self = updateSymbolDictionary(self)                      
        % Update the symbol dictionary used for controls and attacks.
        HOURS_TO_SECONDS = 3600;
        nAttacks = numel(self.attacks);
                
        % get time into the simulation and clocktime and insert in
        % dictionary
        TIME = double(self.simTime)/HOURS_TO_SECONDS;
        self.symbolDict('TIME') = TIME;
        self.symbolDict('CLOCKTIME') = mod(self.startTime+TIME,24);                

        % put attack status symbols in dictionary
        %(i.e. ATT1 = 1, then 1st attack is currently ON)
        for i = 1 : nAttacks
            eval(sprintf('self.symbolDict(''%s%d'') = %d;',...
                'ATT', i,self.attacks{i}.inplace));   
        end
        
        % put component symbols in dictionary for each control
        for i = 1 : numel(self.epanetMap.controls)
            % get the control sensor ID
            thisSensor = EpanetHelper.getComponentId(self.epanetMap.controls(i).nIndex, 1);       
            % change EpanetHelper.getComponentValueForAttacks -_-
            eval(sprintf('self.symbolDict(''%s'') = EpanetHelper.getComponentValueForAttacks(''%s'');',...
                        thisSensor,thisSensor));
                    
        end
        
        
        % put component symbols in dictionary for each attack for both
        % initial
        for i = 1 : nAttacks
            % initial condition
            thisCondition = self.attacks{i}.ini_condition;
            % retrieve vars
            vars = symvar(thisCondition);
            for j = 1 : numel(vars)
                thisVar = vars{j};
                % check if symbol has already been included (search dict
                % keys) MAXLEVELS are never updated, TIME, CLOCKTIME and
                % ATTx are evaluated beforehad.
                if ~strcmp(thisVar,'TIME') && ~strcmp(thisVar,'CLOCKTIME') &&...
                        ~strncmp(thisVar,'ATT',3) && ~strncmp(thisVar,'MAXLEVEL',8)
                    eval(sprintf('self.symbolDict(''%s'') = EpanetHelper.getComponentValueForAttacks(''%s'');',...
                        thisVar,thisVar));
                end
            end
        end
        
        % ... and ending conditions
        for i = 1 : nAttacks
            % initial condition
            thisCondition = self.attacks{i}.end_condition;
            % retrieve vars
            vars = symvar(thisCondition);
            for j = 1 : numel(vars)
                thisVar = vars{j};
                % check if symbol has already been included (search dict
                % keys) MAXLEVELS are never update, TIME, CLOCKTIME and
                % ATTx are evaluated beforehad.
                if ~strcmp(thisVar,'TIME') && ~strcmp(thisVar,'CLOCKTIME') &&...
                        ~strncmp(thisVar,'ATT',3) && ~strncmp(thisVar,'MAXLEVEL',8)
                    eval(sprintf('self.symbolDict(''%s'') = EpanetHelper.getComponentValueForAttacks(''%s'');',...
                        thisVar,thisVar));
                end
            end
        end        
    end   
       
    function [] = overrideControls(self,thisPLC,PLCdict)
        % override control logic by calling control objects in 
        % the epanetMap
        
        % retrieve controls and call overide method
        controls = self.epanetMap.controls;
        for i = 1 : numel(thisPLC.controlsID)
            ix = thisPLC.controlsID(i);
            controls(ix).overrideControl(PLCdict);
        end       
    end
    
    function self = activateDummyControls(self, tstep)
        % complete override by activating dummy controls
        % *** TO DO *** merge with overrideCOntrols?

        % retrieve active dummy controls
        if sum([self.epanetMap.dummyControls.isActive]) > 0            
            for i = 1 : numel(self.attacks)
                if self.attacks{i}.inplace == 1
                    ix = self.attacks{i}.actControl;
                    if ~isempty(ix)
                        lS = self.attacks{i}.setting;
                        time = int64(self.simTime + tstep);
                        self.epanetMap.dummyControls(ix) = ...
                            self.epanetMap.dummyControls(ix).activateWithValues(lS,time);
                    end
                end
            end
        end
    end
    
    function self = storeAlteredReadingEntry(self, layer, target, alteredReading)  
        % creates and store new entry for altered readings
        thisEntry.time     = self.T(end);
        thisEntry.layer    = layer;
        thisEntry.sensorId = target;
        thisEntry.reading  = alteredReading;
        self.alteredReadings = cat(1,self.alteredReadings,thisEntry);
    end
    
    % end of private methods
    end            
end
    