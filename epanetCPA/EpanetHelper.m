classdef EpanetHelper
    % This class contains generic functions to interface with the Epanet
    % toolkit and modify Epanet maps. All the methods are Static so
    % that they can be called without instantiating an object.
    %
    %
    %
    % The MIT License
    % 
    % Copyright (c) 2017 Riccardo Taormina, 
    % Singapore University of Technology and Design.
    % email: riccardo_taormina@sutd.edu.sg, riccardo.taormina@gmail.com
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in
    % all copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    % THE SOFTWARE.
    %   

    
    
    %% TODO: ALL ERRORCODES (in all code) SHOULD HAVE SAME SPELLING FOR CONSISTENCY
    
    
    % Properties
    properties(Constant)
        % list of epanet parameter codes (dictionary)
        EPANETCODES = containers.Map(...
            {...
            'EN_DEMAND',    'EN_HEAD',...
            'EN_PRESSURE',  'EN_FLOW',...
            'EN_STATUS',    'EN_SETTING',...
            'EN_ENERGY',    'EN_TANKLEVEL',...
            'EN_PATCOUNT',  'EN_DURATION',...
            'EN_CONTROLCOUNT', 'EN_BASEDEMAND',...
            'EN_DIAMETER',  'EN_LENGTH', 'EN_ROUGHNESS',...
            'EN_MINLEVEL','EN_MAXLEVEL',...
            'EN_REPORTSTART', ...
            'EN_NODECOUNT', 'EN_LINKCOUNT',...
            'EN_PATTERNSTEP'},...
            [9, 10, 11, 8, 11, 12, ...
            13, 8, 3, 0, 5, 40, 0, ...
            1, 2, 20, 21, 6, 0, 2, ...
            3]);    
    end

    % Public methods
    methods
        function self = EpanetHelper()
            % Empty constructor
        end
    end
    
    % Static methods
    methods(Static)
    
    function errorcode = epanetclose()
        % This code is modified from that of Philip Jonkergouw
        %
        % EPANETCLOSE - close the dll library 
        %
        % Syntax:  [errorcode] = epanetclose()
        %
        % Inputs:
        %    none
        % Outputs:
        %    errorcode - Fault code according to EPANET.
        %
        % Example: 
        %    [errorcode]=epanetclose()
        %        
        % Original version
        % Author: Philip Jonkergouw
        % Email:  pjonkergouw@gmail.com
        % Date:   July 2007

        % Close EPANET ...
        [errorcode] = calllib('epanet2', 'ENclose');
        if (errorcode) fprintf('EPANET error occurred. Code %g\n',...
                num2str(errorcode)); end
        if libisloaded('epanet2') unloadlibrary('epanet2'); end
    end

    function errorcode = epanetloadfile(inpFile)
        % This code is modified from that of Philip Jonkergouw and Demetrios Eliades
        %
        % EPANETLOADFILE - Loads the dll library and the network INP file.
        %
        % Syntax:  [errorcode] = epanetloadfile(inpFile)
        %
        % Inputs:
        %    inpFile - A string, name of the INP file
        %
        % Outputs:
        %    errorcode - Fault code according to EPANET.
        %
        % Example: 
        %    [errorcode]=epanetloadfile('Net1.inp')
        %
        % Original version
        % Author: Philip Jonkergouw
        % Email:  pjonkergouw@gmail.com
        % Date:   July 2007
        %
        % Minor changes by
        % Author: Demetrios Eliades
        % University of Cyprus, KIOS Research Center for Intelligent Systems and Networks
        % email: eldemet@gmail.com
        % Website: http://eldemet.wordpress.com
        % August 2009; Last revision: 21-August-2009

        %------------- BEGIN CODE --------------

        % Load the EPANET 2 dynamic link library ...
        if ~libisloaded('epanet2')
            loadlibrary('epanet2', 'epanet2.h'); 
        end

        % Open the water distribution system ...
        s = which(inpFile);
        if ~isempty(s) inpFile = s; end

        [errorcode] = calllib('epanet2', 'ENopen', inpFile, 'temp1.$$$', 'temp2.$$$');
        if (errorcode)
            fprintf('Could not open network ''%s''.\nReturned empty array.\n', inpFile);
            return;
        else
        end
    end
        
    function list = getComponents(type)
        % Get list of Map components according to type 
        list  = {};

        nComponents = int32(0); isNode = 0;
        switch type        
            % junctions and dummy junctions (for overflow simulation)
            case {'JUNCTIONS', 'OF_JUNCTIONS'}
                [~,nComponents] =...
                    calllib('epanet2','ENgetcount',0,nComponents);
                getTypeFunction = 'ENgetnodetype';        
                componentCode = 0;
                isNode = 1;
                
            % reservoirs
            case {'RESERVOIRS'}
                [~,nComponents] =...
                    calllib('epanet2','ENgetcount',0,nComponents);
                getTypeFunction = 'ENgetnodetype';        
                componentCode = 1;
                isNode = 1;

            % tanks and dummy tanks
            case {'TANKS', 'OF_TANKS'}
                [~,nComponents] =...
                    calllib('epanet2','ENgetcount',0,nComponents);
                getTypeFunction = 'ENgetnodetype';        
                componentCode = 2;
                isNode = 1;
            
            % pipes and dummy pipes (overflow)
            case {'PIPES','OF_PIPES'}
                [~,nComponents] =...
                    calllib('epanet2','ENgetcount',2,nComponents);
                getTypeFunction = 'ENgetlinktype';
                componentCode = 1;
           
            % pumps 
            case 'PUMPS'
                [~,nComponents] =...
                    calllib('epanet2','ENgetcount',2,nComponents);
                getTypeFunction = 'ENgetlinktype';
                componentCode = 2;

            % valves
            case 'VALVES'
                [~,nComponents] =...
                    calllib('epanet2','ENgetcount',2,nComponents);
                getTypeFunction = 'ENgetlinktype';
                componentCode = 3:8;

            otherwise
                error('Search for RESERVOIRS, TANKS, OF_TANKS, JUNCTIONS, OF_JUNCTIONS, PUMPS, VALVES, PIPES, OF_PIPES or all. No %s',type);
        end

        componentType = int32(0);
        for i = 1 : nComponents        
            % retrieve component type
            index = int32(i);        
            [~,componentType] =...
                calllib('epanet2',getTypeFunction,index,componentType);

            if ismember(componentType,componentCode)
                % found component, retrieve its id
                [id,~] = EpanetHelper.getComponentId(index,isNode); 

                % This part of code is to tell apart normal components from
                % dummy components according to how they are named.
                if strcmp('TANKS',type) 
                    % storage tanks
                    if numel(id) >= 2 && ~strcmp(id(1:2),'OF')
                        list = cat(1,list,id);
                    end
                elseif strcmp('OF_TANKS',type) 
                    % dummy tanks
                    if numel(id) >= 2 && strcmp(id(1:2),'OF')
                        list = cat(1,list,id);
                    end
                elseif strcmp('JUNCTIONS',type) 
                    % junctions
                    if numel(id) >= 2 && strcmp(id(1),'J')
                        list = cat(1,list,id);
                    end
                elseif strcmp('OF_JUNCTIONS',type) 
                    % dummy junctions
                    if numel(id) >= 3 && strcmp(id(1:3),'OFj')
                        list = cat(1,list,id);
                    end            
                elseif strcmp('PIPES',type)
                    % pipes
                    if numel(id) >= 3 && ~strcmp(id(1:3),'OFp')
                        list = cat(1,list,id);
                    end
                elseif strcmp('OF_PIPES',type)
                    % dummy pipes
                    if numel(id) >= 3 && strcmp(id(1:3),'OFp')
                        list = cat(1,list,id);
                    end
                else
                    % reservoirs, valves and pumps
                    list = cat(1,list,id);
                end        
            end
        end            
    end
    
    function sections = addDummyTanks(inpFile)
        %% Add dummy tanks to epanet .inp file to simulate overflow.
        % TODO: find a different way to implememnt this coeff.
        % Maybe it needs other artificial components, such as FCV or
        % emitters.

        % get tanks that can overflow (all as for now)
        tanks = EpanetHelper.getComponents('TANKS');
        
        % retrieve .inp file sections
        sections = EpanetHelper.divideInpInSections(inpFile);

        % cycle through all the links and find pipes connected to tanks                
        nLinks = int32(0); 
        [errorcode, nLinks] = calllib('epanet2', 'ENgetcount', 2, nLinks); % get number of links
        
        node1 = int32(0); node2 = int32(0); % initialize nodes at link's ends
        
        for i = 1 : nLinks
            % current link
            thisLink = int32(i);
            
            % get nodes connected to link
            [~, node1,node2] = calllib(...
                'epanet2', 'ENgetlinknodes', thisLink, node1,node2);
            ID1 = EpanetHelper.getComponentId(node1,1);
            ID2 = EpanetHelper.getComponentId(node2,1);

            % check if there is a tank, if so then store tank ID and the ID
            % of the connecting junction
            if ismember(ID1,tanks)        
                thisTank  = ID1;
                thisJunc  = ID2;
            elseif ismember(ID2,tanks)
                thisJunc  = ID1;
                thisTank  = ID2;
            else
                % no tank connected, skip
                continue;
            end
            
            % create labels if there is a tank
            OFtank = ['OF', thisTank];
            OFjunc = ['OFj',thisTank];

            % add lines to sections
            
            % [COORDS]            
            sectionIx = find(cellfun(@(x) strcmp(x,'[COORDINATES]'),{sections.name})); % get section index
            coordLine = EpanetHelper.findLineInSection(...
                sections(sectionIx),thisTank);  % find tank coordinates (for placing dummy tank on the map)

            % extend section with additional lines      
            sections(sectionIx).text = cat(1,sections(sectionIx).text,...
                sprintf(' %s\t %3.3f\t %3.3f\t;',...
                OFtank,str2num(coordLine{2})+10,str2num(coordLine{3})+10)); % dummy tank coords line

            sections(sectionIx).text = cat(1,sections(sectionIx).text,...
                sprintf(' %s\t %3.3f\t %3.3f\t;',...
                OFjunc,str2num(coordLine{2})+10,str2num(coordLine{3})));    % dummy junction coords line
            
            % [JUNC]
            sectionIx = find(cellfun(@(x) strcmp(x,'[JUNCTIONS]'),{sections.name})); % get section index
        
            % extend section with additional lines  
            sections(sectionIx).text = cat(1,sections(sectionIx).text,...
                sprintf(' %s\t %d\t %d\t %s\t;',...
                OFjunc,0,0,' '));

    
            % [TANKS]
            sectionIx = find(cellfun(@(x) strcmp(x,'[TANKS]'),{sections.name})); % get section index
   
            % extend section with additional lines  
            sections(sectionIx).text = cat(1,sections(sectionIx).text,...
                sprintf(' %s\t %d\t %d\t %d\t %d\t %3.3f\t %d\t %s\t;',...
                OFtank,0,0,0,1,2*sqrt(1/pi)*10^3,0,' '));

            % [PIPES]
                            
            % get existing pipe settings            
            pDiam   = EpanetHelper.getComponentValue(thisLink, 0, EpanetHelper.EPANETCODES('EN_DIAMETER'));
            pLength = EpanetHelper.getComponentValue(thisLink, 0, EpanetHelper.EPANETCODES('EN_LENGTH'));
            pRough  = EpanetHelper.getComponentValue(thisLink, 0, EpanetHelper.EPANETCODES('EN_ROUGHNESS'));
            
            sectionIx = find(cellfun(@(x) strcmp(x,'[PIPES]'),{sections.name})); % get section index
        
                
%             % TODO: find a different way to implememnt this coeff. Maybe
%             % need to add a FCV? Or emitters?
%             coeff = 0.1; % this coefficient regulates the amount of overflow 

            % extend section with additional lines                     
            OFp = ['OFp',thisTank];   % twin pipe label
            sections(sectionIx).text = cat(1,sections(sectionIx).text,...
                sprintf(' %s\t %s\t %s\t %0.3f\t %0.3f\t %0.3f\t %d\t %s\t;',...
                OFp, thisJunc, OFjunc, pLength, pDiam, pRough, 0,'Closed'));    % twin pipe                                      
            
            CVp = ['CVp',thisTank]; % name of CV pipe connecting to dummy tank
            sections(sectionIx).text = cat(1,sections(sectionIx).text,...
                sprintf(' %s\t %s\t %s\t %0.3f\t %0.3f\t %0.3f\t %d\t %s\t;',...
                [CVp,'b'],OFjunc,OFtank,1,pDiam,pRough,0,'CV')); % CV pipe     
        end


    end
    
    function [] = createInpFileFromSections(sections,inpFile)
        % Creates EPANET input file from section structarray
        
        % open file (write)
        fileId = fopen(inpFile,'w');
        for i = 1 : numel(sections)
            if i > 1
                % add space
                fprintf(fileId, '\n\n');
            end

            % write section name
            fprintf(fileId, '%s\n', sections(i).name);

            % insert section text
            nLines = numel(sections(i).text);
            for j = 1 : nLines
                thisLine = sections(i).text{j};
                fprintf(fileId, '%s\n', thisLine);
            end
        end
        fclose(fileId);
    end
    
    function sections = divideInpInSections(inpFile)
        % Reads a .inp file and divides it into sections
        
        fileId = fopen(inpFile);
        sections = [];
        thisSection = [];
        while ~feof(fileId)
            thisLine   = fgetl(fileId);
            temp = regexp(thisLine,'\[(.*?)\]','match');
            if ~isempty(temp)

                % add previous section
                sections = cat(1,sections,thisSection);

                % start of a section
                thisSection.name = temp{1};       
                thisSection.text = {};
            else
                % add text to section
                thisSection.text = cat(1,thisSection.text,thisLine);
            end
        end
        % add last section
        sections = cat(1,sections,thisSection);
        fclose(fileId);
    end
    
    function line = findLineInSection(section, id)
        % retrieves line in a [SECTION] pertaining to a given component (id)
        text = section.text;
        nLines = numel(text);
        line = {};
        for i = 1 : nLines
            thisLine = text{i};
            temp = regexp(strtrim(thisLine),'\s*','split');
            if strcmp(temp{1},id)
                line = temp;
                return;
            end
        end

        if isempty(line)
            error('COMPONENT NOT FOUND IN SECTION TEXT');
        end
    end
        
    function [id,errorcode] = getComponentId(index,isNode)
        % Get the ID of an EPANET component, whether node or link

        % initialize
        index = int32(index);
        id = '';
        if isNode 
            [errorcode,id] =...
                    calllib('epanet2', 'ENgetnodeid', index, id);
        else
            [errorcode,id] =...
                calllib('epanet2', 'ENgetlinkid', index, id);   
        end
    end        
    
    function [index,errorcode,isNode] = getComponentIndex(id)
        % Get the index of an EPANET component, whether node or link

        isNode = true;
        [errorcode,id,index] = calllib('epanet2', 'ENgetnodeindex', id, 0);

        if errorcode
            % ... maybe is a link
            [errorcode,~,index] = calllib('epanet2', 'ENgetlinkindex', id, 0);
            isNode = false;
        end
    end
    
    function [value, errorcode] = getComponentValue(index, isNode, code)
        % Get values (according to code) of node or link

        % check if is a node
        value = single(0);
        if isNode
            [errorcode,value] = ...
                calllib('epanet2', 'ENgetnodevalue',...
                int32(index), code, value);
        else
            [errorcode,value] = ...
            calllib('epanet2', 'ENgetlinkvalue',...
            int32(index), code, value);   
        end
    end
    
    function errorcode = setComponentValue(index, value, code)
        % Set values (according to code) of node or link
        
        % set value, try node first
        errorcode = calllib('epanet2', 'ENsetnodevalue',...
            int32(index), code, value);
        
        if errorcode
            % ... maybe is a link
            errorcode = calllib('epanet2', 'ENsetnodevalue',...
            int32(index), code, value);
        end
                         
    end      
    
    function errorcode = setPattern(index, multipliers)
        % set the pattern identified by index
        pPointer  = libpointer('singlePtr',multipliers);
        pLength   = length(multipliers);
        errorcode = calllib('epanet2','ENsetpattern',...
            int32(index),pPointer,int32(pLength));
    end
    
    function value = getComponentValueForAttacks(id)
            % returns sensor reading for selected component 
            % i.e. Tank = water level, Pipe = flow rate, junction = pressure
            
            % get index and type
            [index,errorcode,isNode] = EpanetHelper.getComponentIndex(id);

            if ~errorcode
                % check if is a node or link
                if isNode
                    %% NODE
                    % is it a junction, reservoir or tank?
                    nodeType = int32(0);
                    [~,nodeType] = calllib('epanet2','ENgetnodetype',index,nodeType);
                    % retrieve value         
                    value = single(0);
                    switch nodeType
                        case 0
                            % junction --> PRESSURE
                            [~,value] = calllib(...
                                'epanet2','ENgetnodevalue',index,EpanetHelper.EPANETCODES('EN_PRESSURE'),value);
                        case 1
                            % reservoir --> HEAD
                            [~,value] = calllib(...
                                'epanet2','ENgetnodevalue',index,EpanetHelper.EPANETCODES('EN_HEAD'),value);
                        case 2
                            % tank --> Water level
                            [~,value] = calllib(...
                                'epanet2','ENgetnodevalue',index,EpanetHelper.EPANETCODES('EN_PRESSURE'),value);
                    end
                else
                    %% LINK
                    % is it a pipe, pump or valve?
                    % actually doesn't matter, just return the flow rate       
                    % retrieve value
                    value = single(0);
                    [~,value] = calllib(...
                        'epanet2','ENgetlinkvalue',index,EpanetHelper.EPANETCODES('EN_FLOW'),value);
                end        
            else    
                error('ERROR %d returned', errorcode);    
            end
        end
    
    %% END OF CLASS
    end    
end