classdef PLC
    % Class implementing a PLC.
    %
    %
    % The MIT License
    % 
    % Copyright (c) 2017 Riccardo Taormina, 
    % Singapore University of Technology and Design.
    % email: riccardo_taormina@sutd.edu.sg, riccardo.taormina@gmail.com
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in
    % all copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    % THE SOFTWARE.
    %   
    
    properties
        name                % PLC identifier
        sensors             % List of sensors connected to PLC
        actuators           % List of actuators controlled by PLC
        sensorsIn           % List of sensors used to control the actuators
                            %   but sent by other PLCs.
        controlsID          % List of controls IDs of this PLC (see whether
                            %   transfer all controls in this class rather
                            %   than EpanetMap
    end
        
    methods
    
    % constructor
    function self = PLC(PLCinfo, controls)
        % creates PLC instance from Info
        self.name        = PLCinfo.name;
        self.sensors     = PLCinfo.sensors;
        self.actuators   = PLCinfo.actuators;
        
        % get sensorsIn by reading controls
        self = self.getControls(controls);
    end
   
    % end of public methods
    end
    
    
    % private methods
    methods (Access = private)
    
    function self = getControls(self, controls)
        % get PLC controls from Map list
    
        self.sensorsIn = {}; self.controlsID = [];
        for i = 1 : numel(controls)
            thisActuator = EpanetHelper.getComponentId(controls(i).lIndex, 0);
            if ismember(thisActuator, self.actuators)
                % retrieve control ID
                self.controlsID = cat(1, self.controlsID, i);            
                % see if node is read by PLC or reading is from another PLC
                thisSensor = EpanetHelper.getComponentId(controls(i).nIndex, 1);
                if ~ismember(thisSensor, self.sensors)
                    % is coming from another PLC, add to sensorsIn
                    self.sensorsIn = cat(1, self.sensorsIn, thisSensor);            
                end                
            end
        end
        % remove redundant sensorsIn
        if ~isempty(self.sensorsIn)
            self.sensorsIn = unique(self.sensorsIn);        
        end
    end        

    % end of private methods
    end
    
end
