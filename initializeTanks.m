function initialTankLevels = initializeTanks(filePath)
% Randomly initializes tank levels
%
%
% The MIT License
% 
% Copyright (c) 2017 Riccardo Taormina, 
% Singapore University of Technology and Design.
% email: riccardo_taormina@sutd.edu.sg, riccardo.taormina@gmail.com
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.



% initialize dictionary
initialTankLevels = containers.Map();

% load file
EpanetHelper.epanetloadfile(filePath);

% retrieve epanet Codes
EN_minL = EpanetHelper.EPANETCODES('EN_MINLEVEL');
EN_maxL = EpanetHelper.EPANETCODES('EN_MAXLEVEL');
EN_iniL = EpanetHelper.EPANETCODES('EN_TANKLEVEL'); 

% get tanks
% THIS HAS TO BE MODIFIED. NEED AUTOMATIC RETIEVAL!
TANKS = {'T1','T2','T3','T4','T5','T6','T7'};

% cycle through tanks
for i = 1 : numel(TANKS)

    % get index from id
    tankIndex = EpanetHelper.getComponentIndex(TANKS{i});
    
    % get min and max
    minL = EpanetHelper.getComponentValue(...
        tankIndex, true, EN_minL);
    
    maxL = EpanetHelper.getComponentValue(...
        tankIndex, true, EN_maxL);
    
    % produce an initial level between min and max
    iniL = (maxL-minL)*rand(1) + minL;
    
    % add key to dictionary
    initialTankLevels(TANKS{i}) = iniL;
end

