% Content

epanetCPA  		Folder containing the toolbox
main.m			Main file for demo
initializeTanks.m	Script for initializing tanks
PLC_info_script.m	Script describing cyber-layer (PLCs)
plot_attack_scenario.m	Plotting simulation results
selectAttack.m		Code defining attack scenarios
5patterns_3days.mat	Demand patterns used for demo
CTOWN.inp		CTOWN epanet map
LICENSE			MIT License		
README			This file